class Geometry3D{
    
    public static int cuboid(int r, int c, int h){
        return r*c*h;
    } 
    
    public static int surfaceArea(int r, int c, int h){
        return 2*(r*c+c*h+h*r);
    }
    
    public static int volumepyramid(int r, int c, int h){
        return (r*c*h)/3;
    }
    public static int surfacepyramid(int r, int c, int h){
        return (r*c)+4*(r*h/2);
    }
    public static int volumetetrahedron(int r, int c, int h){
        return (r*h)/3;
    }
    public static double surfacetetrahedron(int r, int c, int h){
        double a = Math.sqrt(h*h+c/2*c/2);
        
        return 4*(a*c/2);
    }
}