class Geometry3DTest{
    public static void main(String[] args){
        Geometry3D geo = new Geometry3D();
        
        int row=6;
        int col=9;
        int height=3;
        
        System.out.println(geo.cuboid(row,col,height));
        System.out.println("Surface area: "+geo.surfaceArea(row,col,height));
        System.out.println("Pyramid volume: "+geo.volumepyramid(row,col,height));
        System.out.println("Pyramid surface area: "+ geo.surfacepyramid(row,col,height));
        System.out.println("Tetrahedron volume: "+geo.volumetetrahedron(row,col,height));
        System.out.println("Tetrahedron surface: "+geo.surfacetetrahedron(row,col,height));
    }
}